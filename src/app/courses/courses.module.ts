import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {CoursesRoutingModule} from './courses-routing.module';
import {ListComponent} from './list/list.component';
import {AddComponent} from './add/add.component';
import {CoursesService} from './courses.service';
import {DataTablesModule} from 'angular-datatables';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {EditComponent} from './edit/edit.component';
import {MatInputModule} from '@angular/material/input';

@NgModule({
  declarations: [
    ListComponent,
    AddComponent,
    EditComponent,
  ],
  providers: [
    CoursesService
  ],
  imports: [
    CommonModule,
    CoursesRoutingModule,
    DataTablesModule,
    ReactiveFormsModule,
    MatInputModule,
    FormsModule,
  ]
})
export class CoursesModule {
}
