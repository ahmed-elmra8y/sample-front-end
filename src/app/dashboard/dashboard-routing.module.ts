import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {DashboardService} from './dashboard.service';
import {DashboardComponent} from './dashboard.component';

const routes: Routes = [
  {path: '', component: DashboardComponent, resolve: {data: DashboardService}},
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DashboardRoutingModule {
}
