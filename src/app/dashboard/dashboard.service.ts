import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {ActivatedRouteSnapshot, Resolve, RouterStateSnapshot} from '@angular/router';
import {BehaviorSubject, Observable} from 'rxjs';

@Injectable()
export class DashboardService implements Resolve<any> {
  onListChange: BehaviorSubject<any>;
  data: any;

  constructor(
    private httpClient: HttpClient
  ) {
    // Set the defaults
    this.onListChange = new BehaviorSubject([]);
  }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<any> | Promise<any> | any {

    return new Promise<void>((resolve, reject) => {
      Promise.all([
        this.list()
      ]).then(
        ([]) => {
          resolve();
        },
        reject
      );
    });
  }

  list(): Promise<any> {
    return new Promise((resolve, reject) => {
        this.httpClient.post('https://nyc-web.hostallover.net/public/api/news', null)
          .subscribe((response: any) => {
            this.data = response.map((item: any) => new DashboardService(item));
            this.onListChange.next(this.data);
            resolve(this.data);
          }, reject);
      }
    );
  }
}
