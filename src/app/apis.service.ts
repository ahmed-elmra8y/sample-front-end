import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class ApisService {

  // domain
  private domain = 'http://127.0.0.1:8000/api';

  constructor(private http: HttpClient) {
  }

  categories(): any {
    return this.http.get(`${this.domain}/categories`);
  }

  courses(): any {
    return this.http.get(`${this.domain}/courses`);
  }

  addCategory(data: any): any {
    return this.http.post(`${this.domain}/categories`, data);
  }

  addCourse(data: any): any {
    return this.http.post(`${this.domain}/courses`, data);
  }

  editCategory(id: any, data: any): any {
    return this.http.put(`${this.domain}/categories/` + id, data);
  }

  editCourse(id: any, data: any): any {
    return this.http.put(`${this.domain}/courses/` + id, data);
  }

  getCategory(id: any): any {
    return this.http.get(`${this.domain}/categories/` + id);
  }

  getCourse(id: any): any {
    return this.http.get(`${this.domain}/courses/` + id);
  }

  deleteCategory(id: any): any {
    return this.http.delete(`${this.domain}/categories/` + id);
  }

  deleteCourse(id: any): any {
    return this.http.delete(`${this.domain}/courses/` + id);
  }

  searchCourses(term: any): any {
    return this.http.get(`${this.domain}/search/courses?key=` + term);
  }

  searchCategories(term: any): any {
    return this.http.get(`${this.domain}/search/categories?key=` + term);
  }

  courseActivity(id: any, data: any): any {
    return this.http.put(`${this.domain}/course/activity/` + id, data);
  }

  categoryActivity(id: any, data: any): any {
    return this.http.put(`${this.domain}/category/activity/` + id, data);
  }

}
