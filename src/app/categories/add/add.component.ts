import {Component, OnInit} from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {Router} from '@angular/router';
import {ApisService} from 'src/app/apis.service';
import {CategoriesService} from '../categories.service';
import {CoursesModel} from '../../courses/courses.model';

@Component({
  selector: 'app-add',
  templateUrl: './add.component.html',
  styleUrls: ['./add.component.css']
})
export class AddComponent implements OnInit {

  form: FormGroup;

  constructor(
    private router: Router,
    private service: CategoriesService,
    private apis: ApisService,
  ) {
    this.form = this.initForm();
  }

  ngOnInit(): void {
  }

  initRequest(): any {
    return {
      name: this.form.value.name,
      is_active: this.form.value.isActive,
    };
  }

  initForm(): FormGroup {
    return new FormGroup({
      name: new FormControl('', [Validators.required]),
      isActive: new FormControl(false, []),
    });
  }

  add(): void {
    this.apis.addCategory(this.initRequest()).subscribe((response: any) => {
      this.router.navigate(['categories']);
    });
  }

}
