import {Component, Inject, OnInit} from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {DOCUMENT} from '@angular/common';
import {ActivatedRoute, Router} from '@angular/router';
import {ApisService} from '../../apis.service';

@Component({
  selector: 'app-edit',
  templateUrl: './edit.component.html',
  styleUrls: ['./edit.component.css']
})
export class EditComponent implements OnInit {

  form: FormGroup;
  category: any;
  categoryId: any;

  constructor(
    @Inject(DOCUMENT) private document: Document,
    private router: Router,
    private apis: ApisService,
    private activatedRoute: ActivatedRoute
  ) {
    this.form = this.initForm();
    this.categoryId = this.activatedRoute.snapshot.paramMap.get('id');
  }

  ngOnInit(): void {
    this.getCategory();
  }

  initForm(): FormGroup {
    return new FormGroup({
      name: new FormControl('', [Validators.required]),
      isActive: new FormControl(false, []),
    });
  }

  updateForm(): void {
    this.form.patchValue({
      name: this.category.name,
      isActive: this.category.is_active,
    });
  }

  initRequest(): any {
    return {
      name: this.form.value.name,
      is_active: this.form.value.isActive,
    };
  }

  getCategory(): void {
    this.apis.getCategory(this.categoryId).subscribe((response: any) => {
      this.category = response.data;
      this.updateForm();
    });
  }

  edit(): void {
    this.apis.editCategory(this.categoryId , this.initRequest()).subscribe((response: any) => {
      this.router.navigate(['categories']);
    });
  }
}
