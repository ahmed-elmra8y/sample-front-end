import {Component, Inject, OnInit} from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {ActivatedRoute, Router} from '@angular/router';
import {DOCUMENT} from '@angular/common';
import {ApisService} from '../../apis.service';

@Component({
  selector: 'app-edit',
  templateUrl: './edit.component.html',
  styleUrls: ['./edit.component.css']
})

export class EditComponent implements OnInit {

  form: FormGroup;
  course: any;
  courseId: any;
  categories: any;

  constructor(
    @Inject(DOCUMENT) private document: Document,
    private router: Router,
    private apis: ApisService,
    private activatedRoute: ActivatedRoute
  ) {
    this.form = this.initForm();
    this.courseId = this.activatedRoute.snapshot.paramMap.get('id');
  }

  ngOnInit(): void {
    this.getCourse();
    this.getCategories();
  }

  getCategories(): void {
    this.apis.categories().subscribe((response: any) => {
      this.categories = response.data;
    });
  }

  initForm(): FormGroup {
    return new FormGroup({
      name: new FormControl('', [Validators.required]),
      categoryId: new FormControl('1', [Validators.required]),
      description: new FormControl('', [Validators.required]),
      hours: new FormControl('', [Validators.required]),
      level: new FormControl('beginner', [Validators.required]),
      isActive: new FormControl(false, []),
    });
  }

  updateForm(): void {
    this.form.patchValue({
      name: this.course.name,
      isActive: this.course.is_active,
      categoryId: this.course.category_id,
      description: this.course.description,
      hours: this.course.hours,
      level: this.course.level,
    });
  }

  initRequest(): any {
    return {
      name: this.form.value.name,
      category_id: this.form.value.categoryId,
      is_active: this.form.value.isActive,
      description: this.form.value.description,
      hours: this.form.value.hours,
      levels: this.form.value.level,
    };
  }

  getCourse(): void {
    this.apis.getCourse(this.courseId).subscribe((response: any) => {
      this.course = response.data;
      this.updateForm();
    });
  }

  edit(): void {
    this.apis.editCourse(this.courseId , this.initRequest()).subscribe((response: any) => {
      this.router.navigate(['courses']);
    });
  }
}
